//
//  ViewController.swift
//  upload
//
//  Created by Администратор on 14.09.2020.
//  Copyright © 2020 Areas. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func uploadAction(_ sender: Any) {
        let url = "http://cinema.areas.su/user/avatar"
        let parametrs = "1482840"
        let headers: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        let img = UIImage(named: "test")
        let urlData = img?.pngData()
        AF.upload(multipartFormData: {multipartFormData in
            multipartFormData.append(parametrs.data(using: .utf8)!, withName: "token")
            multipartFormData.append(urlData!, withName: "file", fileName: "data_0_480x640.png", mimeType: "video/mp4")
        }, to: url, usingThreshold: UInt64.init(), headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                
                print(value)
                
            case .failure(let error):
                print(error)
                
            }
        }
    }
    
}

